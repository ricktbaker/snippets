#!/usr/bin/env ruby
############################################################################
# This script can be useful if you try to delete a security group but get  #
# an error such as "resource has a dependent object".   You'll need the    #
# ruby aws-sdk installed to use this.   Just pass it the security group id #
# that you're trying to delete and it will search through a few things and #
# let you know what it finds                                               #
############################################################################


###
### Configuration - Need to edit the following two lines
###
aws_key = "YOUR_AWS_KEY_GOES_HERE"
aws_sec = "YOUR_AWS_SECRET_KEY_GOES_HERE"

mySG = ARGV[0]

if !mySG
  puts "You must enter a security group ID to look for"
  exit
end

require 'rubygems'

begin
  require 'aws-sdk'
rescue LoadError
  puts "You need to have the ruby aws-sdk installed"
end

AWS.config(
    :access_key_id => aws_key,
    :secret_access_key => aws_sec
  )

@ec2 = AWS::EC2.new
@elb = AWS::ELB.new

puts "Searching EC2 Security Groups For: #{mySG}"
@ec2.security_groups.each do |sGroups|
  sGroups.egress_ip_permissions.each do |perms|
    perms.groups.each do |sGroup|
      puts "Found Reference In Security Group: #{sGroup.id}" if sGroup.id == mySG
    end
  end
  sGroups.ingress_ip_permissions.each do |perms|
    perms.groups.each do |sGroup|
      puts "Found Reference In Security Group: #{sGroup.id}" if sGroup.id == mySG
    end
  end
end

puts "Searching EC2 Instances For: #{mySG}"
@ec2.instances.each do |instance|
  instance.security_groups.each do |sGroup|
    puts "Found Reference in Instance: #{instance.id}" if sGroup.id == mySG
  end
end

puts "Searching ELBs For: #{mySG}"
@elb.load_balancers.each do |thisElb|
  thisElb.security_groups.each do |sGroup|
    puts "Found Reference In ELB: #{elb.id}" if sGroup.id == mySG
  end
end

puts "Searching Network Interfaces For: #{mySG}"
@ec2.network_interfaces.each do |interface|
  interface.security_groups.each do |sGroup|
    puts "Found Reference in Network Interface: #{interface.id}" if sGroup.id == mySG
  end
end