Script for helping track down resource dependent object errors when trying to delete a security group, example:

resource sg-ac9b8200 has a dependent object

Requires, rubygem aws-sdk (centos: yum install rubygem-aws-sdk)

Just save this as security_group_scan.rb and pass it the security group you’re looking for and it will let you know the
actual resource objects that it finds, so you can untangle the dependencies.