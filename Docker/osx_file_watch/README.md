## OS X / Docker Sync Utility

Due to an issue with inotify and mounted OS X volumes in a docker container, you can't easily have your docker node
process reload when developing on the host.

This script will watch for file changes, and sync those changed files up to the proper docker container and will let
the node process reload as it should if using something like nodemon.

If you have an IDE that supports it, like IntelliJ, you can put this script as a startup script so it will startup
whenever you open up your project.

Requires fswatch: https://github.com/emcrisostomo/fswatch