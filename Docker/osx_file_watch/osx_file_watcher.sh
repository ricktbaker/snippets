#!/bin/bash
# Author: rick@ricktbaker.com
# File system watcher to sync changes to docker
# Requires fswatch https://github.com/emcrisostomo/fswatch

## Docker Container Name
CONTAINER_NAME="my_container"

## Docker Source Directory
DOCKER_DIR="/usr/src/app"

## Find our real path this script resides in
realpath () {
  [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

## Change into this directory
cd $(dirname `realpath $0`)

## Assign the fullbasepath to the current directory
fullbasepath="$(pwd)"

## Find our running container
CONTAINER="$(docker ps -a | grep $CONTAINER_NAME | awk '{print $1}')"

sync_files () {
    PASSED_FILE=$1

    ## Get the filename from the base of our project root
    SYNCFILE=${PASSED_FILE#${fullbasepath}}

    echo "Syncing $SYNCFILE to container $CONTAINER"
    CMD="docker cp .$SYNCFILE $API_CONTAINER:$DOCKER_DIR$SYNCFILE";
    $CMD

}

fswatch -0 -r --exclude=.git --exclude=.idea --exclude=node_modules/ --exclude=docker/ --exclude=___* . | while read -d "" event
  do
    sync_files ${event}
  done